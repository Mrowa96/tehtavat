<?php

namespace Domain\Worker\Service;

use Domain\Worker\Entity\Worker;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;

/**
 * Interface WorkerUpdateServiceInterface
 * @package Domain\Worker\Service
 */
interface WorkerUpdateServiceInterface
{
    /**
     * @param Worker $worker
     * @param WorkerUpdateDTO $workerUpdateDTO
     * @return Worker
     */
    public function update(Worker $worker, WorkerUpdateDTO $workerUpdateDTO): Worker;
}
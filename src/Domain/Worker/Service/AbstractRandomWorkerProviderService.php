<?php

namespace Domain\Worker\Service;

use Domain\Task\Entity\Task;
use Domain\Worker\Exception\NoWorkersFoundException;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Domain\Worker\Entity\Worker;
use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;

/**
 * Class AbstractRandomWorkerProviderService
 * @package Domain\Worker\Service
 */
abstract class AbstractRandomWorkerProviderService
{
    /**
     * @var WorkerRepositoryInterface
     */
    protected $workerRepository;

    /**
     * @var WorkerTaskRepositoryInterface
     */
    protected $workerTaskRepository;

    /**
     * AbstractRandomWorkerProviderService constructor.
     * @param WorkerRepositoryInterface $workerRepository
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     */
    public function __construct(
        WorkerRepositoryInterface $workerRepository,
        WorkerTaskRepositoryInterface $workerTaskRepository
    )
    {
        $this->workerRepository = $workerRepository;
        $this->workerTaskRepository = $workerTaskRepository;
    }

    /**
     * @param Task $task
     * @param AbstractWorkersPoolStrategy $workersPoolStrategy
     * @throws NoWorkersFoundException
     * @return Worker
     */
    abstract public function getRandomWorkerForTask(Task $task, AbstractWorkersPoolStrategy $workersPoolStrategy): Worker;
}
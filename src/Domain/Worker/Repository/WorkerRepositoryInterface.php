<?php

namespace Domain\Worker\Repository;

use Domain\Worker\Exception\NoWorkerFoundException;
use Domain\Worker\Exception\WorkerAddException;
use Domain\Worker\Exception\WorkerUpdateException;
use Domain\Worker\Exception\WorkerValidationException;
use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;

/**
 * Interface WorkerRepositoryInterface
 * @package Domain\Worker\Repository
 */
interface WorkerRepositoryInterface
{
    /**
     * @param WorkerAddDTO $workerDTO
     * @throws WorkerAddException
     * @throws WorkerValidationException
     * @return Worker
     */
    public function add(WorkerAddDTO $workerDTO): Worker;

    /**
     * @param int $id
     * @param WorkerUpdateDTO $workerUpdateDTO
     * @throws WorkerUpdateException
     * @throws NoWorkerFoundException
     * @throws WorkerValidationException
     * @return Worker
     */
    public function update(int $id, WorkerUpdateDTO $workerUpdateDTO): Worker;

    /**
     * @param int $id
     * @return Worker|null
     */
    public function getOne(int $id): ?Worker;

    /**
     * @return WorkerCollection
     */
    public function getAll(): WorkerCollection;
}
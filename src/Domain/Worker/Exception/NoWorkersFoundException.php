<?php

namespace Domain\Worker\Exception;

/**
 * Class NoWorkersFoundException
 * @package Domain\Worker\Exception
 */
final class NoWorkersFoundException extends \Exception
{

}
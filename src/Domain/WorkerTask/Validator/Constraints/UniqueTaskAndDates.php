<?php

namespace Domain\WorkerTask\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueTaskAndDates
 * @package Domain\WorkerTask\Validator\Constraints
 *
 * @Annotation
 */
class UniqueTaskAndDates extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Entity with the same task and dates already exists.';

    /**
     * @inheritdoc
     */
    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}
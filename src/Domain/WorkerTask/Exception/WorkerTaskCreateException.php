<?php

namespace Domain\WorkerTask\Exception;

/**
 * Class WorkerTaskCreateException
 * @package Domain\WorkerTask\Exception
 */
final class WorkerTaskCreateException extends \Exception
{

}
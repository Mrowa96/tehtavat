<?php

namespace Domain\WorkerTask\Factory;

use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;

/**
 * Interface WorkerTaskFactoryInterface
 * @package Domain\WorkerTask\Factory
 */
interface WorkerTaskFactoryInterface
{
    /**
     * @param WorkerTaskAddDTO $workerTaskAddDTO
     * @return WorkerTask
     */
    public function create(WorkerTaskAddDTO $workerTaskAddDTO): WorkerTask;

    /**
     * @param WorkerTask[] $data
     * @return WorkerTaskCollection
     */
    public function createCollection(array $data): WorkerTaskCollection;
}
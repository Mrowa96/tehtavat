<?php

namespace Domain\WorkerTask\Entity;

use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;

/**
 * Class WorkerTask
 * @package Domain\WorkerTask\Entity
 */
class WorkerTask
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Worker
     */
    private $worker;

    /**
     * @var Task
     */
    private $task;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * WorkerTask constructor.
     * @param Worker $worker
     * @param Task $task
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(Worker $worker, Task $task, \DateTime $startDate, \DateTime $endDate)
    {
        $this->worker = $worker;
        $this->task = $task;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @param Worker $worker
     */
    public function changeWorker(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * @return Worker
     */
    public function worker(): Worker
    {
        return $this->worker;
    }

    /**
     * @param Task $task
     */
    public function changeTask(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @return Task
     */
    public function task(): Task
    {
        return $this->task;
    }

    /**
     * @param \DateTime $startDate
     */
    public function changeStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime
     */
    public function startDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function changeEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    }
    
    /**
     * @return \DateTime
     */
    public function endDate(): \DateTime
    {
        return $this->endDate;
    }
}
<?php

namespace Domain\WorkerTask\Entity;

use Domain\AbstractCollection;

/**
 * Class WorkerTaskCollection
 * @package Domain\WorkerTask\Entity
 */
class WorkerTaskCollection extends AbstractCollection
{
    /**
     * @return WorkerTask[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
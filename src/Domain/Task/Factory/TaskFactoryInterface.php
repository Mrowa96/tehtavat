<?php

namespace Domain\Task\Factory;

use Domain\Task\Entity\Task;
use Domain\Task\Exception\TaskCreateException;
use Domain\Task\Entity\TaskCollection;
use Infrastructure\Component\Task\DTO\TaskAddDTO;

/**
 * Interface TaskFactoryInterface
 * @package Domain\Task\Factory
 */
interface TaskFactoryInterface
{
    /**
     * @param TaskAddDTO $taskAddDTO
     * @throws TaskCreateException
     * @return Task
     */
    public function create(TaskAddDTO $taskAddDTO): Task;

    /**
     * @param Task[] $data
     * @return TaskCollection
     */
    public function createCollection(array $data): TaskCollection;
}
<?php

namespace Application\DependencyInjection;

use OldSound\RabbitMqBundle\DependencyInjection\Compiler\RegisterPartsPass;
use OldSound\RabbitMqBundle\DependencyInjection\OldSoundRabbitMqExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class AppExtension
 * @package Application\DependencyInjection
 */
final class AppExtension extends Extension
{
    /**
     * @inheritdoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $container->registerExtension(new OldSoundRabbitMqExtension());
        $container->addCompilerPass(new RegisterPartsPass());

        $domainLoader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../Infrastructure/Resources/config'));
        $domainLoader->load('domain.yml');

        $servicesLoad = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $servicesLoad->load('services.yml');
    }
}

<?php

namespace Application\DependencyInjection\Compiler;

use Symfony\Component\Config\Resource\DirectoryResource;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Finder\Finder;

/**
 * Class ValidatorPass
 * @package Application\DependencyInjection\Compiler
 */
final class ValidatorPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $validationFilesPath = __DIR__ . '/../../../Infrastructure/Resources/config/validation';
        $validatorBuilder = $container->getDefinition('validator.builder');
        $validatorFiles = [];
        $finder = new Finder();

        foreach ($finder->files()->in($validationFilesPath) as $file) {
            $validatorFiles[] = $file->getRealPath();
        }

        $validatorBuilder->addMethodCall('addYamlMappings', array($validatorFiles));

        $container->addResource(new DirectoryResource($validationFilesPath));
    }
}
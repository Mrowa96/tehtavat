<?php

namespace Application\Command;

use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateWorkerTaskCommand
 * @package Application\Command
 */
final class UpdateWorkerTaskCommand extends Command
{
    private const ARGUMENT_ID = 'id';
    private const OPTION_WORKER_ID = 'worker-id';
    private const OPTION_TASK_ID = 'task-id';
    private const OPTION_DATE = 'date';

    /**
     * @var WorkerTaskRepositoryInterface
     */
    private $workerTaskRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UpdateWorkerTaskCommand constructor.
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        WorkerTaskRepositoryInterface $workerTaskRepository,
        LoggerInterface $logger
    )
    {
        $this->workerTaskRepository = $workerTaskRepository;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:update-worker-task')
            ->setDescription('Updates existing worker task entity')
            ->addArgument(UpdateWorkerTaskCommand::ARGUMENT_ID, InputArgument::REQUIRED, 'Entity id')
            ->addOption(UpdateWorkerTaskCommand::OPTION_WORKER_ID, null, InputOption::VALUE_REQUIRED)
            ->addOption(UpdateWorkerTaskCommand::OPTION_TASK_ID, null, InputOption::VALUE_REQUIRED)
            ->addOption(UpdateWorkerTaskCommand::OPTION_DATE, null, InputOption::VALUE_REQUIRED);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $workerTaskId = $input->getArgument(UpdateWorkerTaskCommand::ARGUMENT_ID);
            $date = $input->getOption(UpdateWorkerTaskCommand::OPTION_DATE) ?? null;
            $workerTaskUpdateDTO = new WorkerTaskUpdateDTO(
                $input->getOption(UpdateWorkerTaskCommand::OPTION_WORKER_ID),
                $input->getOption(UpdateWorkerTaskCommand::OPTION_TASK_ID),
                !is_null($date) ? (new \DateTime($date))->setTime(0, 0, 0) : null,
                !is_null($date) ? (new \DateTime($date))->setTime(23, 59, 59) : null
            );

            $this->workerTaskRepository->update($workerTaskId, $workerTaskUpdateDTO);

            $output->writeln('<info>Worker task entity updated</info>');
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
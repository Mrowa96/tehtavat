<?php

namespace Application\Service;

/**
 * Interface WorkerTaskAssignConfirmationServiceInterface
 * @package Application\Service
 */
interface WorkerTaskAssignConfirmationServiceInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function send(array $data): bool;

    /**
     * @return string
     */
    public function getOutput(): string;
}
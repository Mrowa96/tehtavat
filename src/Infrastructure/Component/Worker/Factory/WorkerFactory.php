<?php

namespace Infrastructure\Component\Worker\Factory;

use Domain\Worker\Exception\WorkerCreateException;
use Domain\Worker\Factory\WorkerFactoryInterface;
use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;

/**
 * Class WorkerFactory
 * @package Infrastructure\Component\Worker\Factory
 */
final class WorkerFactory implements WorkerFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(WorkerAddDTO $workerDTO): Worker
    {
        try {
            $worker = new Worker(
                $workerDTO->name,
                $workerDTO->email
            );
        } catch (\Throwable $exception) {
            throw new WorkerCreateException($exception);
        }

        return $worker;
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): WorkerCollection
    {
        return new WorkerCollection($data);
    }
}
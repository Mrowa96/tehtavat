<?php

namespace Infrastructure\Component\Worker\DTO;

/**
 * Class WorkerUpdateDTO
 * @package Infrastructure\Component\Worker\DTO
 *
 * @property string|null $name
 * @property string|null $email
 */
class WorkerUpdateDTO
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $email;

    /**
     * WorkerUpdateDTO constructor.
     * @param string|null $name
     * @param string|null $email
     */
    public function __construct(string $name = null, string $email = null)
    {
        $this->name = $name;
        $this->email = $email;
    }
}
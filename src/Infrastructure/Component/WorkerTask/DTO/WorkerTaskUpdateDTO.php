<?php

namespace Infrastructure\Component\WorkerTask\DTO;

use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;

/**
 * Class WorkerTaskUpdateDTO
 * @package Infrastructure\Component\WorkerTask\DTO
 *
 * @property int|null $workerId
 * @property int|null $taskId
 * @property \DateTime|null $startDate
 * @property \DateTime|null $endDate
 */
class WorkerTaskUpdateDTO
{
    /**
     * @var int|null
     */
    public $workerId;

    /**
     * @var int|null
     */
    public $taskId;

    /**
     * @var \DateTime|null
     */
    public $startDate;

    /**
     * @var \DateTime|null
     */
    public $endDate;

    /**
     * WorkerTaskUpdateDTO constructor.
     * @param int $workerId
     * @param int $taskId
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(
        int $workerId = null,
        int $taskId = null,
        \DateTime $startDate = null,
        \DateTime $endDate = null)
    {
        $this->workerId = $workerId;
        $this->taskId = $taskId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
}
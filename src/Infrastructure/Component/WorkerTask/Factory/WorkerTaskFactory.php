<?php

namespace Infrastructure\Component\WorkerTask\Factory;

use Domain\WorkerTask\Exception\WorkerTaskCreateException;
use Domain\WorkerTask\Factory\WorkerTaskFactoryInterface;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;

/**
 * Class WorkerTaskFactory
 * @package Infrastructure\Component\WorkerTask\Factory
 */
final class WorkerTaskFactory implements WorkerTaskFactoryInterface
{

    /**
     * @inheritdoc
     */
    public function create(WorkerTaskAddDTO $workerTaskAddDTO): WorkerTask
    {
        try {
            $workerTask = new WorkerTask(
                $workerTaskAddDTO->worker,
                $workerTaskAddDTO->task,
                $workerTaskAddDTO->startDate,
                $workerTaskAddDTO->endDate
            );
        } catch (\Throwable $exception) {
            throw new WorkerTaskCreateException($exception);
        }

        return $workerTask;
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): WorkerTaskCollection
    {
        return new WorkerTaskCollection($data);
    }
}
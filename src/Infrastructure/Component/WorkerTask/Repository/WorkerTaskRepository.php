<?php

namespace Infrastructure\Component\WorkerTask\Repository;

use Domain\WorkerTask\Exception\WorkerTaskAddException;
use Domain\WorkerTask\Exception\WorkerTaskCreateException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Domain\Task\Entity\Task;
use Domain\WorkerTask\Event\WorkerTaskCreatedEvent;
use Domain\WorkerTask\Exception\WorkerTaskUpdateException;
use Domain\WorkerTask\Exception\WorkerTaskValidationException;
use Domain\WorkerTask\Factory\WorkerTaskFactoryInterface;
use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Domain\WorkerTask\Service\WorkerTaskUpdateServiceInterface;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class WorkerTaskRepository
 * @package Infrastructure\Component\WorkerTask\Repository
 */
final class WorkerTaskRepository implements WorkerTaskRepositoryInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var WorkerTaskFactoryInterface
     */
    private $workerTaskFactory;

    /**
     * @var WorkerTaskUpdateServiceInterface
     */
    private $workerTaskUpdateService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * WorkerRepository constructor.
     * @param EntityManager $entityManager
     * @param WorkerTaskFactoryInterface $workerTaskFactory
     * @param WorkerTaskUpdateServiceInterface $workerTaskUpdateService
     * @param ValidatorInterface $validator
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EntityManager $entityManager,
        WorkerTaskFactoryInterface $workerTaskFactory,
        WorkerTaskUpdateServiceInterface $workerTaskUpdateService,
        ValidatorInterface $validator,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->entityManager = $entityManager;
        $this->workerTaskFactory = $workerTaskFactory;
        $this->workerTaskUpdateService = $workerTaskUpdateService;
        $this->validator = $validator;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritdoc
     */
    public function add(WorkerTaskAddDTO $workerTaskAddDTO): WorkerTask
    {
        try {
            $workerTask = $this->workerTaskFactory->create($workerTaskAddDTO);
        } catch (WorkerTaskCreateException $exception) {
            throw new WorkerTaskAddException('Problem occurred during new worker task creation.');
        }

        $errors = $this->validator->validate($workerTask);

        if ($errors->count()) {
            throw new WorkerTaskValidationException((string)$errors);
        }

        try {
            $this->entityManager->persist($workerTask);
            $this->entityManager->flush();

            $this->eventDispatcher->dispatch(WorkerTaskCreatedEvent::NAME,
                new WorkerTaskCreatedEvent($workerTask)
            );
        } catch (\Exception $exception) {
            throw new WorkerTaskAddException('Problem occurred during saving new worker task.');
        }

        return $workerTask;
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, WorkerTaskUpdateDTO $workerTaskUpdateDTO): WorkerTask
    {
        $workerTask = $this->getOne($id);

        $this->workerTaskUpdateService->update($workerTask, $workerTaskUpdateDTO);

        $errors = $this->validator->validate($workerTask);

        if ($errors->count()) {
            throw new WorkerTaskValidationException((string)$errors);
        }

        try {
            $this->entityManager->persist($workerTask);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new WorkerTaskUpdateException('Problem occurred during saving existing worker task.');
        }

        return $workerTask;
    }

    /**
     * @inheritdoc
     */
    public function delete(int $id): void
    {
        $workerTask = $this->getOne($id);

        if ($workerTask) {
            $this->entityManager->remove($workerTask);
            $this->entityManager->flush();
        }
    }

    /**
     * @inheritdoc
     */
    public function getOne(int $id): ?WorkerTask
    {
        return $this->getBaseQuery()
            ->where('wt.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @inheritdoc
     */
    public function getAllBetweenDates(\DateTime $startDate, \DateTime $endDate): WorkerTaskCollection
    {
        $workersTasks = $this->getBaseQuery()
            ->where('wt.startDate >= :startDate')
            ->andWhere('wt.endDate <= :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getQuery()
            ->getResult();

        return $this->workerTaskFactory->createCollection($workersTasks);
    }

    /**
     * @inheritdoc
     */
    public function getLastFew(int $quantity, bool $reverseOrder = false): WorkerTaskCollection
    {
        $workersTasks = $this->getBaseQuery()
            ->orderBy('wt.id', 'DESC')
            ->setMaxResults($quantity)
            ->getQuery()
            ->getResult();

        if ($reverseOrder === false) {
            $workersTasks = array_reverse($workersTasks);
        }

        return $this->workerTaskFactory->createCollection($workersTasks);
    }

    /**
     * @inheritdoc
     */
    public function getLastFewForTask(Task $task, int $quantity, bool $reverseOrder = false): WorkerTaskCollection
    {
        $workersTasks = $this->getBaseQuery()
            ->where('wt.task = :task')
            ->orderBy('wt.id', 'DESC')
            ->setParameter('task', $task)
            ->setMaxResults($quantity)
            ->getQuery()
            ->getResult();

        if ($reverseOrder === false) {
            $workersTasks = array_reverse($workersTasks);
        }

        return $this->workerTaskFactory->createCollection($workersTasks);
    }

    /**
     * @inheritdoc
     */
    public function hasOneByTaskAndDates(Task $task, \DateTime $startDate, \DateTime $endDate): bool
    {
        $workerTask = $this->getBaseQuery()
            ->where('wt.task = :task')
            ->andWhere('wt.startDate = :startDate')
            ->andWhere('wt.endDate = :endDate')
            ->setParameter('task', $task)
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->getQuery()
            ->getOneOrNullResult();

        return !is_null($workerTask);
    }

    /**
     * @return QueryBuilder
     */
    private function getBaseQuery(): QueryBuilder
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('wt')
            ->from(WorkerTask::class, 'wt');
    }
}
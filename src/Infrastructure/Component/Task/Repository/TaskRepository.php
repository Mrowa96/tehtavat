<?php

namespace Infrastructure\Component\Task\Repository;

use Domain\Task\Exception\TaskCreateException;
use Domain\Task\Exception\TaskAddException;
use Doctrine\ORM\EntityManager;
use Domain\Task\Factory\TaskFactoryInterface;
use Domain\Task\Repository\TaskRepositoryInterface;
use Domain\Task\Entity\Task;
use Domain\Task\Entity\TaskCollection;
use Infrastructure\Component\Task\DTO\TaskAddDTO;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class TaskRepository
 * @package Infrastructure\Component\Task\Repository
 */
final class TaskRepository implements TaskRepositoryInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TaskFactoryInterface
     */
    private $taskFactory;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * TaskRepository constructor.
     * @param EntityManager $entityManager
     * @param TaskFactoryInterface $taskFactory
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManager $entityManager,
        TaskFactoryInterface $taskFactory,
        ValidatorInterface $validator
    )
    {
        $this->entityManager = $entityManager;
        $this->taskFactory = $taskFactory;
        $this->validator = $validator;
    }


    /**
     * @inheritdoc
     */
    public function add(TaskAddDTO $taskAddDTO): Task
    {
        try {
            $task = $this->taskFactory->create($taskAddDTO);
        } catch (TaskCreateException $exception) {
            throw new TaskAddException('Problem occurred during new task creation.');
        }

        $errors = $this->validator->validate($task);

        if ($errors->count()) {
            throw new TaskAddException((string)$errors);
        }

        try {
            $this->entityManager->persist($task);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new TaskAddException('Problem occurred during saving new task.');
        }

        return $task;
    }

    /**
     * @inheritdoc
     */
    public function getAll(): TaskCollection
    {
        $tasks = $this->entityManager
            ->createQueryBuilder()
            ->select('t')
            ->from(Task::class, 't')
            ->getQuery()
            ->getResult();

        return $this->taskFactory->createCollection($tasks);
    }

    /**
     * @inheritdoc
     */
    public function getOne(int $id): ?Task
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('t')
            ->from(Task::class, 't')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @inheritdoc
     */
    public function getOneByName(string $name): ?Task
    {
        $task = $this->entityManager
            ->createQueryBuilder()
            ->select('t')
            ->from(Task::class, 't')
            ->where('t.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();

        return $task;
    }
}
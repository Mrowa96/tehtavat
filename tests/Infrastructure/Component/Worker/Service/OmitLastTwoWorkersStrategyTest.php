<?php

namespace Infrastructure\Component\Worker\Service;

use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class OmitLastTwoWorkersStrategyTest
 * @package Infrastructure\Component\Worker\Service
 */
class OmitLastTwoWorkersStrategyTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider(): array
    {
        $worker1 = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $worker1->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $worker2 = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $worker2->expects($this->any())
            ->method('id')
            ->willReturn(2);

        $worker3 = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $worker3->expects($this->any())
            ->method('id')
            ->willReturn(3);

        $workerTask1 = $this->getMockBuilder(WorkerTask::class)
            ->disableOriginalConstructor()
            ->getMock();
        $workerTask1->expects($this->any())
            ->method('worker')
            ->willReturn($worker1);

        $workerTask2 = $this->getMockBuilder(WorkerTask::class)
            ->disableOriginalConstructor()
            ->getMock();
        $workerTask2->expects($this->any())
            ->method('worker')
            ->willReturn($worker2);

        $workerTask3 = $this->getMockBuilder(WorkerTask::class)
            ->disableOriginalConstructor()
            ->getMock();
        $workerTask3->expects($this->any())
            ->method('worker')
            ->willReturn($worker3);

        $workerCollection = new WorkerCollection([$worker1, $worker2, $worker3]);

        return [
            'one_worker_task' => [
                3,
                $workerCollection,
                $workerTaskCollection = new WorkerTaskCollection([$workerTask1]),
            ],
            'two_worker_tasks' => [
                2,
                $workerCollection,
                $workerTaskCollection = new WorkerTaskCollection([$workerTask1, $workerTask2])
            ],
            'three_worker_tasks' => [
                1,
                $workerCollection,
                $workerTaskCollection = new WorkerTaskCollection([$workerTask1, $workerTask2, $workerTask3])
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     * @param int $quantity
     * @param WorkerCollection $workerCollection
     * @param WorkerTaskCollection $workerTaskCollection
     */
    public function testWithThreeWorkers(int $quantity, WorkerCollection $workerCollection, WorkerTaskCollection $workerTaskCollection)
    {
        $omitLastTwoWorkersStrategy = new OmitLastTwoWorkersStrategy();
        $idsCollection = $omitLastTwoWorkersStrategy->createWorkersPool($workerCollection, $workerTaskCollection);

        $this->assertEquals($quantity, $idsCollection->count());
    }
}
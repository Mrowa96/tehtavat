<?php

namespace Infrastructure\Component\Task\DTO;

use Infrastructure\Component\Task\DTO\TaskAddDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class TaskAddDTOTest
 * @package Infrastructure\Component\Task\DTO
 */
class TaskAddDTOTest extends TestCase
{
    public function testCreate()
    {
        $taskAddDTO = new TaskAddDTO('test name', 'test title');

        $this->assertEquals('test name', $taskAddDTO->name);
        $this->assertEquals('test title', $taskAddDTO->title);
    }
}
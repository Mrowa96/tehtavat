<?php

namespace Infrastructure\Component\Task\Factory;

use Domain\Task\Entity\Task;
use Domain\Task\Entity\TaskCollection;
use Domain\Task\Exception\TaskCreateException;
use Domain\Task\Factory\TaskFactoryInterface;
use Infrastructure\Component\Task\DTO\TaskAddDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class TaskFactoryTest
 * @package Infrastructure\Component\Task\Factory
 */
class TaskFactoryTest extends TestCase
{
    /**
     * @var TaskFactoryInterface $factory
     */
    protected $factory;

    public function setUp()
    {
        $this->factory = new TaskFactory();
    }

    public function testCreateMethodSuccess()
    {
        $taskAddDTO = new TaskAddDTO('test', 'Test');

        $task = $this->factory->create($taskAddDTO);

        $this->assertInstanceOf(TaskFactoryInterface::class, $this->factory);
        $this->assertInstanceOf(Task::class, $task);
        $this->assertEquals('test', $task->name());
        $this->assertEquals('Test', $task->title());
    }

    public function testCreateMethodWithNullableDTO()
    {
        $this->expectException(TaskCreateException::class);

        $taskAddDTO = $this->getMockBuilder(TaskAddDTO::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $this->factory->create($taskAddDTO);
    }

    public function testCreateCollectionMethodSuccess()
    {
        $task1 = new Task('test1', 'Test 1');
        $task2 = new Task('test2', 'Test 2');

        $collection = $this->factory->createCollection([$task1, $task2]);

        $this->assertInstanceOf(TaskCollection::class, $collection);
        $this->assertEquals(2, $collection->count());
        $this->assertSame($task1, $collection->current());

        $collection->next();

        $this->assertSame($task2, $collection->current());
    }
}
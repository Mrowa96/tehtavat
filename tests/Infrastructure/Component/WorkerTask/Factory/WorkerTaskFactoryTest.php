<?php

namespace Infrastructure\Component\WorkerTask\Factory;

use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Domain\WorkerTask\Exception\WorkerTaskCreateException;
use Domain\WorkerTask\Factory\WorkerTaskFactoryInterface;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkerTaskFactoryTest
 * @package Infrastructure\Component\WorkerTask\Factory
 */
class WorkerTaskFactoryTest extends TestCase
{
    /**
     * @var WorkerTaskFactoryInterface $factory
     */
    protected $factory;

    public function setUp()
    {
        $this->factory = new WorkerTaskFactory();
    }

    public function testCreateMethodSuccess()
    {
        $worker = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $worker->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $task = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $task->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $workerTaskAddDTO = new WorkerTaskAddDTO($worker, $task, new \DateTime(), new \DateTime());

        $workerTask = $this->factory->create($workerTaskAddDTO);

        $this->assertInstanceOf(WorkerTaskFactoryInterface::class, $this->factory);
        $this->assertInstanceOf(WorkerTask::class, $workerTask);
        $this->assertEquals($worker->id(), $workerTask->worker()->id());
        $this->assertEquals($task->id(), $workerTask->task()->id());
    }

    public function testCreateMethodWithNullableDTO()
    {
        $this->expectException(WorkerTaskCreateException::class);

        $workerTaskAddDTO = $this->getMockBuilder(WorkerTaskAddDTO::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $this->factory->create($workerTaskAddDTO);
    }

    public function testCreateCollectionMethodSuccess()
    {
        $worker = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $worker->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $task = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $task->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $workerTask1 = new WorkerTask($worker, $task, new \DateTime(), new \DateTime());
        $workerTask2 = new WorkerTask($worker, $task, new \DateTime(), new \DateTime());

        $collection = $this->factory->createCollection([$workerTask1, $workerTask2]);

        $this->assertInstanceOf(WorkerTaskCollection::class, $collection);
        $this->assertEquals(2, $collection->count());
        $this->assertSame($workerTask1, $collection->current());

        $collection->next();

        $this->assertSame($workerTask2, $collection->current());
    }
}
<?php

namespace Infrastructure\Component\WorkerTask\DTO;

use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkerTaskAddDTOTest
 * @package Infrastructure\Component\WorkerTask\DTO
 */
class WorkerTaskAddDTOTest extends TestCase
{
    public function testCreate()
    {
        $worker = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $task = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $startDate = new \DateTime();
        $endDate = new \DateTime();

        $workerTaskAddDTO = new WorkerTaskAddDTO($worker, $task, $startDate, $endDate);

        $this->assertEquals($worker, $workerTaskAddDTO->worker);
        $this->assertEquals($task, $workerTaskAddDTO->task);
        $this->assertEquals($startDate, $workerTaskAddDTO->startDate);
        $this->assertEquals($endDate, $workerTaskAddDTO->endDate);
    }
}